import datetime
import hashlib
import random
from datetime import date
from fastapi import FastAPI, Body
from fastapi import Request, Response
from fastapi.responses import FileResponse, RedirectResponse
from fastapi.exceptions import HTTPException
from sqlalchemy import create_engine, select, update
from starlette import status
from starlette.staticfiles import StaticFiles

from CreateTable import Employee, Authorization, Salary

engine = create_engine('sqlite:///company.db')

app = FastAPI()

app.mount("/static", StaticFiles(directory='./', html=True), name='static')


@app.get('/')
def hello_world(request: Request):
    auth_token = request.cookies.get("auth_token")
    if auth_token is None:
        return RedirectResponse("/login")
    else:
        stmt = select(Authorization) \
            .where(Authorization.auth_token == auth_token)
        with engine.connect() as conn:
            row = conn.execute(stmt).first()
            if row is None:
                return RedirectResponse("/login",
                                        status_code=status.HTTP_303_SEE_OTHER)
            response = FileResponse("main_page/index.html")

            if row[3] < datetime.datetime.now().timestamp():
                refresh_token = request.cookies.get("refresh_token")
                if refresh_token == row[2]:
                    employee_id: int = row[0]
                    refresh_user_token(employee_id)

                    stmt = select(Authorization) \
                        .where(Authorization.employee_id == employee_id)
                    row = conn.execute(stmt).first()
                    auth_token = row[1]
                    refresh_token = row[2]
                    token_expiration = row[3]

                    response.set_cookie(key="auth_token",
                                        value=auth_token, httponly=True)
                    response.set_cookie(key="refresh_token",
                                        value=refresh_token, httponly=True)
                    response.set_cookie(key="token_expiration",
                                        value=token_expiration, httponly=True)
                else:
                    return RedirectResponse("/login")
    return response


@app.get('/salary')
def salary(request: Request):
    auth_token = request.cookies.get("auth_token")
    stmt = select(Authorization).where(Authorization.auth_token == auth_token)
    with engine.connect() as conn:
        row = conn.execute(stmt).first()
        if row is None:
            return RedirectResponse("/login")
        employee_id: int = row[0]
        stmt = select(Salary).where(Salary.employee_id == employee_id)
        row = conn.execute(stmt).first()
        salary_value = row[1]
        promotion_date = row[2]
        stmt = select(Employee.fio).where(Employee.id == employee_id)
        row = conn.execute(stmt).first()
        fio = row[0]
    return {"fio": fio, "salary": salary_value,
            "promotion_date": promotion_date}


@app.get('/exit')
def logout():
    response = RedirectResponse("/login",
                                status_code=status.HTTP_303_SEE_OTHER)
    response.set_cookie(key="auth_token", value="", httponly=True)
    response.set_cookie(key="refresh_token", value="", httponly=True)
    response.set_cookie(key="token_expiration", value="", httponly=True)
    return response


@app.get('/login')
def login():
    return FileResponse("./login/index.html")


@app.post('/auth')
def authorisation(data: str = Body()):
    items = data.split("&")
    values = {}
    for item in items:
        key_value = item.split("=")
        values[key_value[0]] = key_value[1]
    stmt = select(Employee.id) \
        .where(Employee.login == values["login"]) \
        .where(Employee.password == values["password"])
    employee_id: int
    with engine.connect() as conn:
        row = conn.execute(stmt).first()
        if row is None:
            response = RedirectResponse("/login?wrong=true",
                                        status_code=status.HTTP_303_SEE_OTHER)
            return response
        employee_id = row[0]
        refresh_user_token(employee_id)
        stmt = select(Authorization) \
            .where(Authorization.employee_id == employee_id)
        row = conn.execute(stmt).first()
        auth_token = row[1]
        refresh_token = row[2]
        token_expiration = row[3]
        response = RedirectResponse("/", status_code=status.HTTP_303_SEE_OTHER)
        response.set_cookie(key="auth_token",
                            value=auth_token, httponly=True)
        response.set_cookie(key="refresh_token",
                            value=refresh_token, httponly=True)
        response.set_cookie(key="token_expiration",
                            value=token_expiration, httponly=True)
    return response


def refresh_user_token(employee_id: int):
    token_raw = random.randint(1, 10) * str(employee_id) + \
                random.randint(1, 10) * date.today().strftime("%d-%B-%Y")
    sha256 = hashlib.sha256()
    sha256.update(token_raw.encode("utf-8"))
    auth_token = sha256.hexdigest()

    token_raw = random.randint(1, 10) * str(employee_id) + \
        random.randint(1, 10) * date.today().strftime("%Y-%B-%d")
    sha256 = hashlib.sha256()
    sha256.update(token_raw.encode("utf-8"))
    refresh_token = sha256.hexdigest()
    token_expiration = int((datetime.datetime.now() +
                            datetime.timedelta(minutes=5)).timestamp())

    stmt = update(Authorization)\
        .where(Authorization.employee_id == employee_id) \
        .values({"auth_token": auth_token,
                 "refresh_token": refresh_token,
                 "token_expiration": token_expiration})
    with engine.connect() as conn:
        conn.execute(stmt)
        conn.commit()


@app.exception_handler(404)
def not_found_exception_handler(request: Request, ex: HTTPException):
    return RedirectResponse("/")


@app.exception_handler(405)
def not_found_exception_handler(request: Request, ex: HTTPException):
    return RedirectResponse("/")


def main():
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8000)

if __name__ == '__main__':
    main()
