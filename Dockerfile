FROM python:3.10.0

WORKDIR /app

RUN pip install poetry

COPY poetry.lock pyproject.toml /app/

RUN poetry config virtualenvs.create false && \
    poetry install --no-interaction --no-ansi
	
COPY . /app/

EXPOSE 8000
CMD ["poetry","run","python","main.py"]