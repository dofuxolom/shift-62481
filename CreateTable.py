from sqlalchemy import create_engine, Column, Integer, String, Date
from sqlalchemy.orm import declarative_base
from sqlalchemy.orm import sessionmaker

Base = declarative_base()


class Employee(Base):
    __tablename__ = 'employees'
    id = Column(Integer, primary_key=True, autoincrement=True)
    login = Column(String, unique=True)
    password = Column(String)
    fio = Column(String)


class Authorization(Base):
    __tablename__ = 'authorization'
    employee_id = Column(Integer, primary_key=True)
    auth_token = Column(String)
    refresh_token = Column(String)
    token_expiration = Column(Integer)


class Salary(Base):
    __tablename__ = 'salary'
    employee_id = Column(Integer, primary_key=True)
    amount = Column(Integer)
    promotion_date = Column(Date)


if __name__ == '__main__':
    engine = create_engine('sqlite:///company.db')
    Base.metadata.create_all(engine)

    Session = sessionmaker(bind=engine)
    session = Session()